function traerConceptos(){
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/obtenerConceptos", false);
	xhr.send();

	var respuesta = xhr.responseText;

	console.log(respuesta);
	return JSON.parse(respuesta);
}

var conceptos = traerConceptos();
var tiempoLimite;
var jugadores;
var jugadorActual = 1;
var jugadaActual = {};
jugadaActual.jugador = 1;
var timer;
var contadorTemporizador;
var funcionTemporizador;
var temporizadorAlerta;


function generarConcepto(){

	function getRandom(max) {
  		return Math.floor(Math.random()*max)+0;
	}

	var numGenerado = getRandom(conceptos.length);
	console.log(numGenerado);

	var concepto = {};
	concepto.nombre =  conceptos[numGenerado].nombre;
	concepto.descripcion = conceptos[numGenerado].descripcion;
	concepto.id = conceptos[numGenerado].id;

	console.log("---------");
	console.log(JSON.stringify(concepto));
	return concepto;
}

function configurarJuego () {
	jugadores = document.getElementById("inputCantJugadores").value;
	tiempoLimite = document.getElementById("inputTiempoLimite").value * 1000;
	contadorTemporizador = tiempoLimite/1000;
	console.log(jugadores);
	var titulo = "";
	var scores = "";
	for (var i = 1; i <= jugadores; i++){
		titulo = titulo + '<th>Jugador '+i+'</th>';
		scores = scores + '<td id="score'+i+'">0</td>';
	}
	var contenido = '<thead class="thead-inverse"><tr>'+titulo+'</tr></thead><tr>'+scores+'</tr>';
	document.getElementById("tablaPuntuaciones").innerHTML = contenido;
	rellenarInfo();
}

function rellenarInfo(){
	clearInterval(funcionTemporizador);
	clearTimeout(timer);
	var concepto = generarConcepto();
	document.getElementById("nombreConcepto").innerHTML = concepto.nombre;
	document.getElementById("descripcion").innerHTML = concepto.descripcion;
	document.getElementById("jugadorActual").innerHTML = "Equipo en turno: "+ jugadaActual.jugador;
	console.log(jugadores);
	jugadaActual.categoriaId = concepto.id;
	/*jugadaActual.jugador = jugadorActual;
	jugadorActual++;
	if(jugadorActual>jugadores){
		jugadorActual = 1;
	}*/
	timer = setTimeout(cambiarTurno,tiempoLimite);
	//funcionTemporizador = setInterval(temporizador,1000);
}

function jugar(id){
	var categoriaSeleccionada = id.substring(id.length-1, id.length);
	console.log(categoriaSeleccionada);
	if(categoriaSeleccionada == jugadaActual.categoriaId){
		document.getElementById("score"+jugadaActual.jugador).innerHTML = parseInt(document.getElementById("score"+jugadaActual.jugador).innerHTML)+1;
		cambiarTurno();
		rellenarInfo();
		audioCorrecto();
	}else {
		mostrarAlerta();
		cambiarTurno();
		audioIncorrecto();
	}
}

function cambiarTurno(){
	clearInterval(funcionTemporizador);
	clearTimeout(timer);
	jugadaActual.jugador++;
	if(jugadaActual.jugador>jugadores){
		jugadaActual.jugador = 1;
	}
	document.getElementById("jugadorActual").innerHTML = "Equipo en turno: "+ jugadaActual.jugador;
	//temporizador = setInterval(temporizador,1000);
	timer = setTimeout(cambiarTurno,tiempoLimite);
	
}

function audioCorrecto () {
	document.getElementById("contenedorAlerta").innerHTML ='<div id="alerta" class="alert alert-success alert-dismissible fade show text-center" role="alert">Respuesta Correcta!</div>';
	temporizadorAlerta = setTimeout(ocultarAlerta,1500);
	document.getElementById("audioCorrecto").play();
}

function audioIncorrecto () {
	document.getElementById("contenedorAlerta").innerHTML ='<div id="alerta" class="alert alert-danger alert-dismissible fade show text-center" role="alert">Respuesta Incorrecta!</div>';
	temporizadorAlerta = setTimeout(ocultarAlerta,1500);
	document.getElementById("audioIncorrecto").play();
}

function mostrarAlerta(){
	
}
function ocultarAlerta(){
	$("#alerta").alert("close");
	clearTimeout(temporizadorAlerta);
}
/*
function temporizador() {
	document.getElementById("contadorTemporizador").innerHTML = "Tiempo restante: " + contadorTemporizador;
	contadorTemporizador--;
	if (contadorTemporizador==0){
		contadorTemporizador = tiempoLimite/1000;
	}
}
*/