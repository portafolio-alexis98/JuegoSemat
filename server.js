var gsjson = require("google-spreadsheet-to-json");
var http = require("http");
var fs = require("fs");
var path = require("path");
var mimeTypes = {
    '.html': 'text/html',
    '.css': 'text/css',
    '.jpg': 'image/jpeg',
    '.png': 'image/png',
    '.js': 'application/javascript',
    '.gif': 'image/gif',
    '.wav': 'audio/x-wav',
    '.ico': 'image/x-icon'
}
var conceptos = [];
var configuracion;

gsjson({
    spreadsheetId: '1ymfZLWYNrY5WBTdkORwhWsdQJmdHTTNT_eA-Xvulxe8'
}).then(function(result) {
    //var updateBD = JSON.parse(result);
    console.log("--------------");
    //console.log(updateBD);
    console.log(result);
    fs.writeFile('BD.json', JSON.stringify(result), function () {
        console.log("BD escrita");
    });
    //console.log(result);
    fs.readFile("BD.json", cargarConceptos);
    function cargarConceptos (error, data) {
    if (error == null){
        conceptos = JSON.parse(data);
    }else {
        console.log(error);
    }
    console.log("Banco cargado!");
    //console.log(JSON.stringify(conceptos));
}
    console.log(result.length);
}).catch (function(err) {
    console.log(err.message);
    console.log(err.stack);
});


var server = http.createServer(atenderServidor);
console.log("Servidor iniciado");
server.listen(process.env.PORT || 5000);

function atenderServidor(request, response){
	console.log("Peticion: "+ request.url);
	var peticion = request.url;
	if(peticion == "/obtenerConceptos"){
		enviarConceptos(request, response);
	} else if (peticion == "/descargarConfiguracion"){
        enviarConfiguracion(request, response);
    } else if (peticion == "/guardarConfiguracion"){
        guardarConfiguracion(request, response);
    } else if (peticion == "/"){
        request.url = "/configuracion.html";
        retornarArchivo(request, response);
    } else {
		retornarArchivo(request, response);
	}
}

function enviarConceptos (request, response) {
	//console.log(JSON.stringify(conceptos));
	response.end(JSON.stringify(conceptos));
}

function enviarConfiguracion (request, response) {
    console.log(JSON.stringify(configuracion));
    response.end(JSON.stringify(configuracion));
}


function retornarArchivo(request, response) {
    var ext = path.extname(request.url.substring(1));
    fs.readFile("." + request.url, recibirArchivo);

    function recibirArchivo(error, data) {
        if (error == null) {
            console.log("path " + ext);
            var fileType = mimeTypes[ext];
            console.log("tipo:" + fileType);
            response.writeHead(200, { 'content-type': fileType });
            response.write(data);
            response.end();
        } else {
            console.log(error);
            response.end(error.toString());
        }
    }
}

function guardarConfiguracion(request, response){
    request.on("data",recibirDatos);
    function recibirDatos (data) {
        configuracion = JSON.parse(data.toString());
        response.end("Configuracion guardada!");
    }
}
